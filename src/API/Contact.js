import React from "react";
import axios from "axios";

class Contact extends React.Component {
  state = {};

  componentDidMount() {
    // {
    //   name: 'ram',
    //     email:"ram@gmail.com"
    // }
    // axios.get()
    axios({
      method: "GET",
      url: "http://localhost:5000/api/v1/contacts",
    })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  handlegetConatc = () => {
    axios({
      method: "GET",
      url: "http://localhost:5000/api/v1/contacts",
      params: {
        name: "ram",
        phone: 929292,
      },
      headers: {
        abc: "bbb",
      },
    })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handlePostContact = () => {
    let postData = {
      name: "ram",
      email: "ram@gmail.com",
      // contact:[{po}]
    };
    axios
      .post("http://localhost:5000/api/v1/contacts", {
        name: "Ram",
        phone: 93883838,
        email: "ram@gmail.com",
      })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleDeleteContact = () => {
    axios
      .delete("http://localhost:5000/api/v1/contacts/60405da8941e045fa4ba25f0")
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };
  handleUpdateContact = () => {
    axios
      .put("http://localhost:5000/api/v1/contacts/60405e3e941e045fa4ba25f1", {
        name: "hari",
        phone: 93883838,
        email: "hari@gmail.com",
      })
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };

  handleGetAllContact = async () => {
    const contact = await axios.all([
      axios.get("http://localhost:5000/api/v1/contacts"),
      axios.get("http://localhost:5000/api/v1/contactsssss"),
      axios.get("http://localhost:5000/api/v1/contacts"),
      axios.get("http://localhost:5000/api/v1/contacts"),
    ]);
    console.log(contact);
  };

  render() {
    return (
      <>
        <button onClick={this.handlegetConatc}>Get</button>
        <button onClick={this.handlePostContact}>Post</button>
        <button onClick={this.handleDeleteContact}>Delete</button>
        <button onClick={this.handleUpdateContact}>Update</button>
        <button onClick={this.handleGetAllContact}>Get All</button>
      </>
    );
  }
}

export default Contact;
