import React from "react";
import ApiCalls from "../hooks/ApiCalls";
import Counter from "../hooks/Counter";
import MemoHooks from "../hooks/MemoHooks";

class Home extends React.Component {
  state = {};
  render() {
    return (
      <>
        <button onClick={this.props.onHandleUpdate}>Update</button> <Counter />
        <ApiCalls />
        <MemoHooks />
      </>
    );
  }
}

export default Home;
