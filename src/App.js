import React from "react";
import Home from "./components/Home";
import { Link, Route, Switch } from "react-router-dom";
import About from "./components/About";
import Contact from "./API/Contact";
import Form from "./formik/Form";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      contact: 92929292,
    };
    console.log("Constructor Run");
  }

  handleUpdate = () => {
    console.log("Called");
    // this.state.contact = 737373773;
    this.setState({ contact: 262662 });
  };
  componentDidMount() {
    // ajax call
    console.log("Component did mount runn...");
  }

  componentDidUpdate(prepProps, prepState) {
    // console.log("Component did updated Run");
    // if (prepState.contact !== this.state.contact) {
    // }
    // console.log(prepState);
    // console.log(prepProps);
  }
  shouldComponentUpdate(nextProp, nextState) {
    console.log(nextProp);
    console.log(nextState);
    if (nextState.contact !== this.state.contact) return true;
    return false;
  }
  render() {
    console.log("Render run");
    return (
      <>
        {" "}
        <Link to="/about">About</Link> <br />
        <Link to="/">Home</Link>
        <Link to="/contact">Contact</Link>
        <h1>{this.state.contact}</h1>{" "}
        <Switch>
          <Route
            path="/"
            exact
            render={(props) => <Home onHandleUpdate={this.handleUpdate} />}
          />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contact} />
          <Route path="/form" component={Form} />
        </Switch>
      </>
    );
  }
}

export default App;
