import React, { useMemo, useState } from "react";

function MemoHooks() {
  const [count, setCount] = useState(0);
  const [count1, setCount1] = useState(0);

  const handleUpdate = () => {
    setCount(count + 1);
  };
  const handleUpdate1 = () => {
    setCount1(count1 + 1);
  };

  const calCount = useMemo(() => {
    let i = 0;
    while (i < 100000000) i++;
    return count > 10;
  }, [count]);
  return (
    <React.Fragment>
      <h3>{count}</h3>
      <h3>{count1}</h3>
      <h1>{calCount ? "Pass" : "fail"}</h1>
      <button onClick={handleUpdate}>Update</button>
      <button onClick={handleUpdate1}>Update1</button>
    </React.Fragment>
  );
}

export default MemoHooks;
