import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";

function ApiCalls() {
  const [postData, setPostData] = useState();

  const apiCall = useCallback(() => {
    axios.get("https://jsonplaceholder.typicode.com/todos/1").then((res) => {
      setPostData(res.data);
    });
  }, []);

  // {} === {}
  useEffect(() => {
    apiCall();
  }, [apiCall]);

  console.log(postData);

  return <h1>Api calls</h1>;
}

export default ApiCalls;
