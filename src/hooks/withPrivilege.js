import React from "react";

function withPrivilege(WrapperComponent) {
  class Hoc extends React.Component {
    constructor() {
      super();
      this.state = {
        privileList: {
          canRead: true,
          canUpdate: false,
        },
      };
    }
    handleUpdatePrivile = () => {};
    render() {
      return (
        <WrapperComponent
          privileList={this.state.privileList}
          update={this.handleUpdatePrivile}
        />
      );
    }
  }

  return Hoc;
}

export default withPrivilege;
