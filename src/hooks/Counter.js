import React, { useEffect, useState } from "react";
import useToggle from "./useToggle";
import withPrivilege from "./withPrivilege";

const Counter = () => {
  // usestate hooks
  const [count, setCount] = useState(0);
  const [istoggele, handleToggle] = useToggle(false);
  const handleUpdateCount = () => {
    setCount(count + 1);
  };
  //   mounting phase
  useEffect(() => {
    console.log("Useeffect calls");

    //   componentWillUnMount similar
    return () => {
      console.log("CleanUp");
    };
  }, []);

  // updated phase
  useEffect(() => {
    console.log("Useeffect calls 1");
  }, [count]);

  return (
    <>
      <h2>You {istoggele ? "Like" : "Dislike"} </h2>
      <button onClick={handleToggle}>Toggle</button>
      <h1 onClick={handleUpdateCount}>Your counter is {count} </h1>
    </>
  );
};

export default withPrivilege(Counter);
