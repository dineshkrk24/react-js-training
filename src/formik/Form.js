import { Formik, Field, ErrorMessage, Form } from "formik";
import React from "react";
import { initialVal, validationSchema } from "./schema";

function Forms() {
  const handleFormSubmit = (values, resetForm) => {
    console.log(values);
    //   resetForm()
  };
  return (
    <>
      <h1>Formik Form</h1>
      <Formik
        enableReinitialize
        initialValues={initialVal}
        onSubmit={(values, { resetForm }) => {
          handleFormSubmit(values, resetForm);
        }}
        validationSchema={validationSchema}
      >
        {(props) => {
          return (
            <>
              <Form>
                <label htmlFor="">Name:</label>
                <input
                  onChange={(e) => {
                    console.log(e.target.value);
                    props.setFieldValue("name", e.target.value);
                  }}
                  onBlur={props.handleBlur}
                  type="text"
                  name="name"
                  placeholder="Name"
                  value={props.values.name}
                />
                <br />
                {props.errors.name && props.touched.name && (
                  <span style={{ color: "red" }}>{props.errors.name}</span>
                )}
                {/* <Field name="name" placeholder="Name" />
                <ErrorMessage
                  name="name"
                  component="div"
                  style={{ color: "red" }}
                /> */}
                <Field name="email" placeholder="Name" />
                <ErrorMessage
                  name="email"
                  component="div"
                  style={{ color: "red" }}
                />
                <Field type="number" name="phoneNumber" placeholder="Name" />
                <ErrorMessage
                  name="phoneNumber"
                  component="div"
                  style={{ color: "red" }}
                />
                <div role="group" aria-labelledby="my-radio-group">
                  <label>
                    <Field type="radio" name="isActive" value="false" />
                    One
                  </label>
                  <label>
                    <Field type="radio" name="isActive" value="true" />
                    Two
                  </label>
                  <div>Picked: {props.values.picked}</div>
                </div>
                <Field type="text" name="address" placeholder="Name" />
                <ErrorMessage
                  name="address"
                  component="div"
                  style={{ color: "red" }}
                />
                <button type="submit">Submit</button>
                {/* <label htmlFor="">Email:</label>
                <input
                  onChange={props.handleChange}
                  onBlur={props.handleBlur}
                  type="text"
                  name="email"
                  placeholder="email"
                  value={props.values.email}
                />
                <br />
                {props.errors.email && props.touched.email && (
                  <span style={{ color: "red" }}>{props.errors.email}</span>
                )}
                <label htmlFor="">Phone Number:</label>
                <input
                  onChange={props.handleChange}
                  onBlur={props.handleBlur}
                  type="number"
                  name="phoneNumber"
                  placeholder="phoneNumber"
                  value={props.values.phoneNumber}
                />
                <br />
                {props.errors.phoneNumber && props.touched.phoneNumber && (
                  <span style={{ color: "red" }}>
                    {props.errors.phoneNumber}
                  </span>
                )} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default Forms;
