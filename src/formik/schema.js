import * as Yup from "yup";

export const initialVal = {
  name: "",
  email: "",
  phoneNumber: "",
  address: "",
  isActive: "false",
};

export const validationSchema = Yup.object().shape({
  name: Yup.string().required("This is requred field!"),
  email: Yup.string().email().required("This is requred field!"),
  //   email: Yup.string()
  //     .matches(
  //       " /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$/",
  //       "Email must be valid "
  //     )
  //     .required("This is requred field!"),
  phoneNumber: Yup.number()
    .min(10, "Must be 10")
    .max(12, "must be less than 12")
    .required("This is requred field!"),
  address: Yup.string().when("isActive", {
    is: "false",
    then: Yup.string().required("This is requred field"),
  }),
});
